Compile time parser combinator for types.
-----------------------------------------

This project includes some helpers and a few tiny parsers that can be used to parse a list of types
at compile time. This is an unfinished toy project with no practical use.

Inspired by [Understanding Parser Combinators](https://fsharpforfunandprofit.com/posts/understanding-parser-combinators/).


