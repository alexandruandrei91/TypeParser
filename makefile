appname := parser

CXX := clang++
CXXFLAGS := -std=c++1z

src := $(shell find . -name "*.cpp")
obj  := $(patsubst %.cpp, %.o, $(src))

all: $(appname)

$(appname): $(obj)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $(appname) $(obj) $(LDLIBS)

depend: .depend

.depend: $(src)
	rm -f ./.depend
	$(CXX) $(CXXFLAGS) -MM $^>>./.depend;

clean:
	rm -f $(obj)

dist-clean: clean
	rm -f *~ .depend

include .depend
