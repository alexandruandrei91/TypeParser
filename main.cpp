#include <type_traits>
#include <iostream>


template <class T>
struct show_type;

namespace parser
{
    // utilities

    template <template<typename...> typename F, typename... ARGS>
    struct apply
    {
        using type = F<ARGS...>;
    };

    // A few specializations to avoid a compiler bug:
    // http://open-std.org/jtc1/sc22/wg21/docs/cwg_active.html#1430
    template <template<typename> typename F, typename ARG1>
    struct apply <F, ARG1>
    {
        using type = F<ARG1>;
    };
    template <template<typename, typename> typename F, typename ARG1, typename ARG2>
    struct apply <F, ARG1, ARG2>
    {
        using type = F<ARG1, ARG2>;
    };
    template <template<typename, typename, typename> typename F, typename ARG1, typename ARG2, typename ARG3>
    struct apply <F, ARG1, ARG2, ARG3>
    {
        using type = F<ARG1, ARG2, ARG3>;
    };
    template< template<typename, typename, typename, typename> typename F
            , typename ARG1, typename ARG2, typename ARG3, typename ARG4>
    struct apply <F, ARG1, ARG2, ARG3, ARG4>
    {
        using type = F<ARG1, ARG2, ARG3, ARG4>;
    };
    template< template<typename, typename, typename, typename, typename> typename F
            , typename ARG1, typename ARG2, typename ARG3, typename ARG4, typename ARG5>
    struct apply <F, ARG1, ARG2, ARG3, ARG4, ARG5>
    {
        using type = F<ARG1, ARG2, ARG3, ARG4, ARG5>;
    };

    template <template<typename...> typename F, typename... ARGS>
    using apply_t = typename apply<F, ARGS...>::type;


    template <template<typename...> typename F, typename... ARGS>
    struct curry
    {
        template <typename... ARGS2>
        using type = apply_t<F, ARGS..., ARGS2...>;
    };


    template <template<typename...> typename F, template<typename...> typename... G>
    struct compose
    {
        template <typename... ARGS>
        using type = apply_t<F, typename compose<G...>::template type<ARGS...>>;
    };

    template <template<typename...> typename F>
    struct compose<F>
    {
        template <typename... ARGS>
        using type = apply_t<F, ARGS...>;
    };

    //template <template<typename...> typename F, template<typename...> typename... G>
    //using compose_t = typename compose<F, G...>::type;


    // Empty list_t.
    struct nil {};

    template <typename X, typename XS>
    struct cons
    {
        using type = cons<X, cons<X, XS>>;
    };

    template <typename X>
    struct cons<X, nil>
    {
        using type = nil;
    };


    template <typename... XS>
    struct list
    {};

    template <typename X, typename... XS>
    struct list<X, XS...>
    {
        using type = cons<X, typename list<XS...>::type>;
    };

    template <>
    struct list<>
    {
        using type = nil;
    };

    template <typename... XS>
    using list_t = typename list<XS...>::type;


    template <typename XS>
    struct head
    {};

    template <typename X, typename XS>
    struct head<cons<X, XS>>
    {
        using type = X;
    };

    template<typename XS>
    using head_t = typename head<XS>::type;


    template <typename XS>
    struct tail
    {};

    template <typename X, typename XS>
    struct tail<cons<X, XS>>
    {
        using type = XS;
    };

    template <>
    struct tail<list_t<>>
    {
        using type = list_t<>;
    };

    template<typename XS>
    using tail_t = typename tail<XS>::type;

    template <template <typename A, typename B> typename F, typename X, typename XS>
    struct foldr
    {
        using type = F<head_t<XS>, typename foldr<F, X, tail_t<XS>>::type>;
    };

    template <template <typename A, typename B> typename F, typename X>
    struct foldr<F, X, list_t<>>
    {
        using type = X;
    };

    template <template <typename A, typename B> typename F, typename X, typename XS>
    using folr_t = typename foldr<F, X, XS>::type;

    template <template <typename A, typename B> typename F, typename X, typename XS>
    struct foldl
    {
        using type = typename foldl<F, F<X, head_t<XS>>, tail_t<XS>>::type;
    };

    template <template <typename A, typename B> typename F, typename X>
    struct foldl<F, X, list_t<>>
    {
        using type = X;
    };

    template <template <typename A, typename B> typename F, typename X, typename XS>
    using foldl_t = typename foldl<F, X, XS>::type;


    template <template <typename A, typename B> typename F, typename XS>
    struct reduce
        : foldl<F, head_t<XS>, tail_t<XS>>
    {};

    template <template <typename A, typename B> typename F, typename XS>
    using reduce_t = typename reduce<F, XS>::type;

    template <template <typename A> typename F, typename XS>
    struct map
    {
        using type = cons<F<head_t<XS>>, typename map<F, tail_t<XS>>::type>;
    };

    template <template <typename A> typename F>
    struct map<F, list_t<>>
    {
        using type = list_t<>;
    };

    template <template <typename A> typename F, typename XS>
    using map_t = typename map<F, XS>::type;


    template <typename FIRST, typename SECOND>
    struct pair
    {
        using first = FIRST;
        using second = SECOND;
    };

    template <typename PAIR>
    using first = typename PAIR::first;

    template <typename PAIR>
    using second = typename PAIR::second;


    // combinators
    template <typename P, typename XS>
    using parse = typename P::template in<XS>::type;

    template <typename P1, typename P2>
    struct and_then
    {
        template <typename XS, typename = std::void_t<>>
        struct in
        {
            static constexpr bool value = false;
        };

        // If P2(P1.output.remaining).output exists, it means that both parsers suceeded.
        template <typename XS>
        struct in < XS
                  , std::void_t<parse<P2, second<parse<P1, XS>>>>>
        {
            static constexpr bool value = true;

        private:
            using p1 = parse<P1, XS>;
            using p2 = parse<P2, second<p1>>;

        public:
            using type = pair< pair<first<p1>, first<p2>>
                             , second<p2>
                             >;
        };
    };

    template <typename P1, typename P2>
    struct or_else
    {
        template <typename XS, typename = std::void_t<>>
        struct in
            : P2::template in<XS>
        {
        };

        template <typename XS>
        struct in <XS, std::void_t<parse<P1, XS>>>
            : P1::template in<XS>
        {
        };
    };

    template <typename PS>
    using choose = reduce_t<or_else, PS>;

    template <template<typename> typename F, typename P>
    struct pmap
    {
        template <typename XS, typename = std::void_t<>>
        struct in
        {
            static constexpr bool value = false;
        };

        template <typename XS>
        struct in <XS, std::void_t<parse<P, XS>>>
        {
            static constexpr bool value = true;

        private:
            using p = parse<P, XS>;

        public:
            using type = pair<apply_t<F, first<p>>, second<p>>;
        };
    };


    // parsers
    template <typename T>
    struct match_type
    {
        template <typename XS>
        struct in
        {
            static constexpr bool value = false;
        };

        template <typename XS>
        struct in <cons<T, XS>>
        {
            static constexpr bool value = true;
            using type = pair<T, XS>;
        };
    };

    template <typename XS>
    using any_of = choose<map_t<match_type, XS>>;

    template <typename T>
    struct preturn
    {
        template <typename XS>
        struct in
        {
            static constexpr bool value = true;
            using type = pair<T, XS>;
        };
    };

    template <typename FP, typename XP>
    struct papply
    {
    private:
        template <typename PAIR>
        struct apply_pair
        {
        };

        template <template<typename> typename FIRST, typename SECOND>
        struct apply_pair <pair<FIRST<SECOND>, SECOND>>
        {
            using type = apply_t<FIRST, SECOND>;
        };

        template <typename PAIR>
        using apply_pair_t = typename apply_pair<PAIR>::type;

    public:
        template <typename XS>
        struct in
            : pmap<apply_pair_t, and_then<FP, XP>>::template in<XS>
        {
        };
    };
}

// https://fsharpforfunandprofit.com/posts/understanding-parser-combinators/

template <typename P, bool EXPECTED, typename... ARGS>
void test(const std::string& name)
{
    using parser = typename P::template in<parser::list_t<ARGS...>>;
    std::cout << (parser::value == EXPECTED ? "(PASS) " : "(FAIL) ")
              << parser::value << " - " << EXPECTED << " "
              << name << "\n";

    if constexpr(EXPECTED != parser::value)
        show_type<typename parser::type> t;
}

template <typename>
struct Z{};

int main()
{
    // TODO:
    // Make it for template <auto A>
    // Separate parsers and combinators
    // Generalize parser
    // Do not expose utility functions (parsers will receive ARGS... not list_t<ARGS...>)
    // Investigate the bind function
    // Composition operator

    struct A{};
    struct B{};
    struct C{};
    struct D{};
    struct E{};

    using p_A = parser::match_type<A>;
    using p_B = parser::match_type<B>;
    using p_C = parser::match_type<C>;
    using p_D = parser::match_type<D>;
    using p_E = parser::match_type<E>;

    using AandB = parser::and_then< p_A, p_B >;
    test<AandB, true, A, B, C, D>("AandB ABCD");
    test<AandB, false, A, A, B, C>("AandB AABC");
    test<AandB, false, A>("AandB A");
    test<AandB, false>("AandB");

    using AorB = parser::or_else< p_A, p_B >;
    test<AorB, true, A, C, D>("AorB ACD");
    test<AorB, true, B, C, D>("AorB BCD");
    test<AorB, true, A>("AorB A");
    test<AorB, true, B>("AorB B");
    test<AorB, false, C, D>("AorB CD");
    test<AorB, false>("AorB");

    using BorC = parser::or_else< p_B, p_C >;
    using AandBorC = parser::and_then< p_A, BorC >;
    test<AandBorC, true, A, B, E>("AandBorC ABE");
    test<AandBorC, true, A, C, E>("AandBorC ACE");
    test<AandBorC, false, D, B, E>("AandBorC DBE");
    test<AandBorC, false, A, D, E>("AandBorC ADE");

    using chooseABC = parser::choose<parser::list_t<p_A, p_B, p_C>>;
    test<chooseABC, true, A, D, E>("chooseABC ADE");
    test<chooseABC, true, B, D, E>("chooseABC BDE");
    test<chooseABC, true, C, D, E>("chooseABC CDE");
    test<chooseABC, true, C>("chooseABC C");
    test<chooseABC, false, E, D, E>("chooseABC EDE");
    test<chooseABC, false>("chooseABC");

    using anyABC = parser::any_of<parser::list_t<A, B, C>>;
    test<anyABC, true, A, D, E>("anyABC ADE");
    test<anyABC, true, B, D, E>("anyABC BDE");
    test<anyABC, true, C, D, E>("anyABC CDE");
    test<anyABC, true, C>("anyABC C");
    test<anyABC, false, E, D, E>("anyABC EDE");
    test<anyABC, false>("anyABC");


    using pmapZ = parser::pmap<Z, anyABC>;
    test<pmapZ, true, A, D, E>("pmapZ ADE");
    test<pmapZ, true, B, D, E>("pmapZ BDE");
    test<pmapZ, true, C, D, E>("pmapZ CDE");
    test<pmapZ, true, C>("pmapZ C");
    test<pmapZ, false, E, D, E>("pmapZ EDE");
    test<pmapZ, false>("pmapZ");

    return 0;
}
